//
//  MockConstants.swift
//  camlKitTests
//
//  Created by Simon Mcloughlin on 25/01/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import Foundation
@testable import camlKit

public struct MockConstants {
	
	// MARK: - Wallets
	
	public static let mnemonic = "remember smile trip tumble era cube worry fuel bracket eight kitten inform"
	public static let passphrase = "superSecurePassphrase"
	public static let messageToSign = "something very interesting that needs to be signed"
	
	public static let defaultLinearWallet = LinearWallet.create(withMnemonic: MockConstants.mnemonic, passphrase: "")!
	public static let defaultHdWallet = HDWallet.create(withMnemonic: MockConstants.mnemonic, passphrase: "")!
	
	public struct linearWalletEd255519 {
		public static let address = "tz1T3QZ5w4K11RS3vy4TXiZepraV9R5GzsxG"
		public static let privateKey = "80d4e52897c8e14fbfad4637373de405fa2cc7f27eb9f890db975948b0e7fdb0cd33a22f74d8e04977f74db15a0b1e92d21a59f351e987b9fd462bf6ef2dc253"
		public static let publicKey = "cd33a22f74d8e04977f74db15a0b1e92d21a59f351e987b9fd462bf6ef2dc253"
		public static let signedData = "c4d20c77d627d8c07e3f26ddc2e8ab9324471c65f9abd412de70a81c21ddc153dcfad1b31ab777a83c4e8a5dc021ea30d84da107dea4a192fc2ca9da9b3ede00"
		public static let base58Encoded = "edpkvCbYCa6d6g9hEcK6tvwgsY9jfB4HDzp3jZSBwfuWNSvxE5T5KR"
	}
	
	public struct linearWalletEd255519_withPassphrase {
		public static let address = "tz1hQ4wkVfNAh3eGeaDpoTBmQ9KjX9ZMzc6q"
		public static let privateKey = "b17877f6b326bf75e8a5bf2bd7e457a03b103d469c869ef4e3b0473d9b9d50b1482c29dcbfc1f94c185e9d8da1ee7e06b16239a5d4e15a64a6f4150c298ab029"
		public static let publicKey = "482c29dcbfc1f94c185e9d8da1ee7e06b16239a5d4e15a64a6f4150c298ab029"
	}
	
	public struct linearWalletSecp256k1 {
		public static let address = "tz2UiZQJwaVAKxRuYxV8Tx5k8a64gZx1ZwYJ"
		public static let privateKey = "80d4e52897c8e14fbfad4637373de405fa2cc7f27eb9f890db975948b0e7fdb0"
		public static let publicKey = "032460b1fb47abc6b64bfa313efdba92eb4313f58b90ac30b68851b4880cc9c819"
	}
	
	public struct hdWallet {
		public static let address = "tz1bQnUB6wv77AAnvvkX5rXwzKHis6RxVnyF"
		public static let privateKey = "3f54684924468bc4a7044729ee578176aef5933c69ad16ff3340412e4a5ca396"
		public static let publicKey = "1e4291f2501ce283e55ce583d4388ec8d247dd6c72fff3ff2d48b2af84cc9a23"
		public static let signedData = "3882ed4ec04ca29e12f3aecea23f50297f8c569c21092dfcef3196eff55ac39a637c2da93d348935a88110f30b177c052f17c432d888a06c9eba434cc3707601"
		public static let base58Encoded = "edpktsYtWnwnuiLkHySdNSihWEChdeFzyz9v8Vdb8aAWRibsPH7g7E"
	}
	
	public struct hdWallet_withPassphrase {
		public static let address = "tz1dcAi3sVtc5xLyNu53avkEiPvhi9cN2idj"
		public static let privateKey = "f76414eebdfea880ada1dad22186f150335547a124d64a65a10fbda25d70dac1"
		public static let publicKey = "9ce8d8d68df863ed6e2c7a8172726f3bd71ddf7e968ba0095bfdf549fea7d67c"
	}
	
	public struct hdWallet_non_hardened {
		public static let address = "tz1gr7B9AsXJMMSBg7NugNvDZpGgvcvX4Q8M"
		public static let privateKey = "2cd4e64b16e9bb9fa7c19aa272bd2dba0d9ae3ee3f764e15351ab7ec11fc37ad"
		public static let publicKey = "caa6f27ebb93808caf3ee7ecbff0fc482bbde9e123293bf34d78dbca105e02c2"
		public static let derivationPath = "m/44'/1729'/0/0"
	}
	
	public struct hdWallet_hardened_change {
		public static let address = "tz1iKnE1sKYvB6F42XAKAf9iR6YauEv2ENzZ"
		public static let privateKey = "8ad51941e26afcdd6408ff59d94188449180bdbda71d8529fafd1948eca8af04"
		public static let publicKey = "ace4ba9cc11a961a78324700ed36feb0277ee005d7cb79ccdb821503029bc672"
		public static let derivationPath = "m/44'/1729'/0'/1'"
	}
}
