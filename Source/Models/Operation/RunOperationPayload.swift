//
//  RunOperationPayload.swift
//  camlKit
//
//  Created by Simon Mcloughlin on 25/08/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation

public struct RunOperationPayload: Codable {
	
	enum CodingKeys: String, CodingKey {
        case chainID = "chain_id"
		case operation
    }
	
	let chainID: String
	let operation: OperationPayload
}
