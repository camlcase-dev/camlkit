//
//  BetterCallDevTokenBalances.swift
//  camlKit
//
//  Created by Simon Mcloughlin on 25/03/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import Foundation

public struct BetterCallDevTokenBalances: Codable {
	public let balances: [BetterCallDevTokenBalance]
}

public struct BetterCallDevTokenBalance: Codable {
	
	public let balance: TokenAmount
	public let contract: String
	public let decimals: Int?
	public let name: String?
	public let symbol: String?
	
	enum CodingKeys: String, CodingKey {
		case balance
		case contract
		case decimals
		case name
		case symbol
	}
	
	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		contract = try container.decode(String.self, forKey: .contract)
		name = try container.decodeIfPresent(String.self, forKey: .name)
		symbol = try container.decodeIfPresent(String.self, forKey: .symbol)
		
		let numberOfDecimals = try container.decodeIfPresent(Int.self, forKey: .decimals) // for avoiding calling self before init finished
		decimals = numberOfDecimals ?? 0
		
		let balanceString = try container.decode(String.self, forKey: .balance)
		balance = TokenAmount(fromRpcAmount: balanceString, decimalPlaces: numberOfDecimals ?? 0) ?? TokenAmount.zeroBalance(decimalPlaces: numberOfDecimals ?? 0)
	}
	
	public func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(contract, forKey: .contract)
		try container.encode(decimals, forKey: .decimals)
		try container.encode(balance.rpcRepresentation, forKey: .balance)
		try container.encode(name, forKey: .name)
		try container.encode(symbol, forKey: .symbol)
	}
}
